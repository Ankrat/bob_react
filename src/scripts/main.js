var React = require('react');
var Catalog = React.createFactory(require('../views/catalog.jsx'));

window.onload = function() {
    React.render(Catalog(), document.getElementById('component'));
};