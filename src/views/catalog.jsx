var React = require('react');
var Catalog = React.createClass({

  render: function() {
    return (
      <div>
        <h3>Catalog</h3>
        <div>
          <button onClick={this._add}>Add to Cart</button>
        </div>
      </div>
    )
  },

  _add: function() {
    console.log('added to your cart!');
  }

});

module.exports = Catalog;